//
//  HomeViewController.swift
//  cheers
//
//  Created by admin on 21/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Kingfisher


class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var PostTableView: UITableView!
    var currUser:User!
    
    private var posts = [Post]()
    var selectedUser:User?
    var Selected:String! 
    var observer:Any?;
    var observerEdit:Any?;
    var observerDelete:Any?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PostTableView.refreshControl = UIRefreshControl()
        PostTableView.refreshControl?.addTarget(self, action: #selector(ReloadData), for: .valueChanged)

        PostTableView.dataSource = self
        PostTableView.delegate = self
        
        currUser = Model.instance.getCurrentUser()
        self.title = "Cheers, " + currUser.username
        
        observer = ModelEvents.NewPostNotification.observe{
            self.PostTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
        
        observerEdit = ModelEvents.EditPostNotification.observe{
            self.PostTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
             
        observerDelete = ModelEvents.DeletePostNotification.observe{
            self.PostTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
        
        self.PostTableView.refreshControl?.beginRefreshing()
        self.ReloadData()
    }
    
    @objc func ReloadData()
    {
        Model.instance.getAllPosts{ (data:[Post]?) in
            if (data != nil) {
                self.posts = data!
                self.PostTableView.reloadData()
            }
            self.PostTableView.refreshControl?.endRefreshing()
        }
    }
    
    deinit{
        if let observer = observer{
            ModelEvents.removeObserver(observer: observer)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PostTableViewCell =
            PostTableView.dequeueReusableCell(withIdentifier: "PostItem")as! PostTableViewCell
        cell.username.text = posts[indexPath.row].username
        cell.date.text = posts[indexPath.row].postTime
        cell.PsotContent.text = posts[indexPath.row].Content
        cell.UserImage.image = UIImage(named: "Image")
        if (posts[indexPath.row].postImage != ""){
            cell.UserImage.kf.setImage(with: URL(string: posts[indexPath.row].postImage))
        }
        //cell.UserImage.image = currUser.avatarImg
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "PostToUserSegue"){
            let vc : ViewUserViewController = segue.destination as! ViewUserViewController
            vc.user = selectedUser
    }
    }
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath){
        Selected = posts[indexPath.row].username
        Model.instance.VerifyUser(username: Selected, callback : { (data:User?) in
            if (data != nil)
            {
                self.selectedUser = data!
            }
            
            self.performSegue(withIdentifier: "PostToUserSegue", sender: self)

        })
        
    }
}
