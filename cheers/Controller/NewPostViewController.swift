//
//  NewPostViewController.swift
//  cheers
//
//  Created by admin on 18/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var content: UITextField!
    @IBOutlet weak var picBtn: UIButton!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var addPostBtn: UIButton!
    @IBOutlet weak var postingSpinner: UIActivityIndicatorView!
    
    var selectedImage:UIImage?
    var contentText:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postingSpinner.isHidden = true;
        self.content.delegate = self
    }
    
    @IBAction func AddNewPost(_ sender: Any) {
        
        contentText = content.text
        if (contentText == "")
        {
            let alert = UIAlertController(title:"Error", message:"Missing content", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else
        {
            let currUser = Model.instance.getCurrentUser()
            let currUname = currUser.username

            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d, hh:mm:ss a"
            let dateString = formatter.string(from: Date())
            
            if let selectedImage = selectedImage{
            postingSpinner.isHidden = false;
            addPostBtn.isEnabled = false;
            picBtn.isEnabled = false;
            Model.instance.storeImage(image: selectedImage, username: currUname) {(url) in
            Model.instance.addPost(post: Post(username: currUname,
                                              time: dateString, content: self.contentText!, image: url, unixtime: 0))
            self.navigationController?.popViewController(animated: true)}
            }
             else {
            postingSpinner.isHidden = false;
            addPostBtn.isEnabled = false;
            picBtn.isEnabled = false;
            Model.instance.addPost(post: Post(username: currUname,
            time: dateString, content: self.contentText!, image: "", unixtime: 0))
            
            self.navigationController?.popViewController(animated: true)
            }
        }
        
        
    }
    
    // Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Presses return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
        @IBAction func choosePic(_ sender: Any) {
            if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerController.SourceType.photoLibrary) {
               let imagePicker = UIImagePickerController()
               imagePicker.delegate = self;
               imagePicker.sourceType =
               UIImagePickerController.SourceType.photoLibrary;
               imagePicker.allowsEditing = true
               self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
        // When user select image in gallery
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage;
            self.postImage.image = selectedImage;
            dismiss(animated: true, completion: nil);
        }
    }

