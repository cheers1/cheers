//
//  ProfileViewController.swift
//  cheers
//
//  Created by admin on 21/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var beer: UILabel!
    @IBOutlet weak var cellphone: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!

    
    var currUser:User!
    var observer: Any?
    
    @IBAction func logout(_ sender: Any) {
         Model.instance.deleteCurrentUser()
               
               let loginVC = storyboard?.instantiateViewController(identifier: "LoginVC")
               
               view.window?.rootViewController = loginVC
               view.window?.makeKeyAndVisible()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currUser = Model.instance.getCurrentUser()
        
        username.text = currUser.username
        city.text = currUser.city
        beer.text = currUser.beer
        cellphone.text = currUser.cell
        profilePicture.image = UIImage(named: "avatar")

        if (currUser?.avatarImg != ""){
            profilePicture.kf.setImage(with: URL(string: currUser!.avatarImg))
        }
        
        observer = ModelEvents.EditCurrentUser.observe{
            self.currUser = Model.instance.getCurrentUser()
            
            self.username.text = self.currUser.username
            self.city.text = self.currUser.city
            self.beer.text = self.currUser.beer
            self.cellphone.text = self.currUser.cell
            self.profilePicture.image = UIImage(named: "avatar")

            if (self.currUser?.avatarImg != ""){
                self.profilePicture.kf.setImage(with: URL(string: self.currUser!.avatarImg))
            }
        }
    }
}
