import UIKit
import Kingfisher

class MyPostsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var MyPostsTableView: UITableView!
    var currUser:User!
    private var posts = [Post]()
    var observer:Any?;
    var observerDelete:Any?;
    var observerEdit:Any?;
    var selectedPost:Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyPostsTableView.refreshControl = UIRefreshControl()
        MyPostsTableView.refreshControl?.addTarget(self, action: #selector(ReloadData), for: .valueChanged)

        MyPostsTableView.dataSource = self
        MyPostsTableView.delegate = self
        
        currUser = Model.instance.getCurrentUser()
        
        observer = ModelEvents.NewPostNotification.observe{
            self.MyPostsTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
        
        observerEdit = ModelEvents.EditPostNotification.observe{
            self.MyPostsTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
        
         observerDelete = ModelEvents.DeletePostNotification.observe{
            self.MyPostsTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
        
        self.MyPostsTableView.refreshControl?.beginRefreshing()
        self.ReloadData()
    }
    
    @objc func ReloadData()
    {
        Model.instance.getAllPosts{ (data:[Post]?) in
            if (data != nil) {
                self.posts = data!
                self.posts = self.posts.filter({ (Post) -> Bool in
                    {
                        Post.username == self.currUser.username
                    }()
                })
                
                self.MyPostsTableView.reloadData()
            }
            self.MyPostsTableView.refreshControl?.endRefreshing()
        }
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MyPostTableViewCell =
            MyPostsTableView.dequeueReusableCell(withIdentifier: "MyPostItem", for: indexPath)as! MyPostTableViewCell
        cell.postId = posts[indexPath.row].postId
        cell.postDate.text = posts[indexPath.row].postTime
        cell.postContent.text = posts[indexPath.row].Content
        cell.postImage.image = UIImage(named: "Image")
          if (posts[indexPath.row].postImage != ""){
              cell.postImage.kf.setImage(with: URL(string: posts[indexPath.row].postImage))
          }
          //cell.UserImage.image = currUser.avatarImg
          
          return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "EditPostSegue"){
            let vc : EditPostViewController = segue.destination as! EditPostViewController
            vc.post = selectedPost
    }
}
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath){
        selectedPost = posts[indexPath.row]
            
        self.performSegue(withIdentifier: "EditPostSegue", sender: self)

    }

}


