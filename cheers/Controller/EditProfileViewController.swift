//
//  EditProfileViewController.swift
//  cheers
//
//  Created by admin on 18/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var beer: UITextField!
    @IBOutlet weak var cellphone: UITextField!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var picBtn: UIButton!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var savingSpinner: UIActivityIndicatorView!
    @IBOutlet weak var DeleteBtn: UIButton!
    
    var currUser:User!
    var EditedUser:User!
    var selectedImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        savingSpinner.isHidden = true;
        currUser = Model.instance.getCurrentUser()
        
        username.text = currUser.username
        password.text = currUser.password
        city.text = currUser.city
        beer.text = currUser.beer
        cellphone.text = currUser.cell
        profilePicture.image = UIImage(named: "avatar")

        if (currUser?.avatarImg != ""){
            profilePicture.kf.setImage(with: URL(string: currUser!.avatarImg))
        }
        
        self.password.delegate = self
        self.city.delegate = self
        self.beer.delegate = self
        self.cellphone.delegate = self
    }
    
    @IBAction func SavePeofileEditing(_ sender: Any) {
        let newPassword = password.text!
        let newCity = city.text!
        let newBeer = beer.text!
        let newCellphone = cellphone.text!
        
        if let selectedImage = selectedImage{
           savingSpinner.isHidden = false;
           saveBtn.isEnabled = false;
           picBtn.isEnabled = false;
           Model.instance.storeImage(image: selectedImage, username: username.text!) {(url) in
            self.EditedUser = User (username: self.currUser.username, password: newPassword, cell: newCellphone, city: newCity, beer: newBeer, avatarImg: url)
                
            Model.instance.updateUser(user: self.EditedUser)
            Model.instance.insertCurrentUser(currU: self.EditedUser)
                ModelEvents.EditCurrentUser.post();
                
            self.navigationController?.popViewController(animated: true)
               }
       }
        else {
            savingSpinner.isHidden = false;
            saveBtn.isEnabled = false;
            picBtn.isEnabled = false;
            
            self.EditedUser = User (username: self.currUser.username, password: newPassword, cell: newCellphone, city: newCity, beer: newBeer, avatarImg: currUser!.avatarImg)
            
            Model.instance.updateUser(user: self.EditedUser)
            Model.instance.insertCurrentUser(currU: self.EditedUser)
                ModelEvents.EditCurrentUser.post();
                
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Presses return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    @IBAction func choosePic(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(
        UIImagePickerController.SourceType.photoLibrary) {
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self;
           imagePicker.sourceType =
           UIImagePickerController.SourceType.photoLibrary;
           imagePicker.allowsEditing = true
           self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // When user select image in gallery
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage;
        self.profilePicture.image = selectedImage;
        dismiss(animated: true, completion: nil);
    }
    
    @IBAction func deleteUser(_ sender: Any) {
        
        let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete your profile?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in self.deleteProfile()}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

        self.present(alert, animated: true)
        
    }
    

    func deleteProfile()
    {
        Model.instance.deleteUser(username: currUser.username)
        Model.instance.deleteCurrentUser()
        
        let loginVC = storyboard?.instantiateViewController(identifier: "LoginVC")
        
        view.window?.rootViewController = loginVC
        view.window?.makeKeyAndVisible()    }
}
