//
//  UsersViewController.swift
//  cheers
//
//  Created by admin on 18/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Kingfisher

class UsersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var UsersTableView: UITableView!
    
    private var users = [User]()
    var selectedUser:User?
    var observer: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UsersTableView.refreshControl = UIRefreshControl()
        UsersTableView.refreshControl?.addTarget(self, action: #selector(ReloadData), for: .valueChanged)
        
        UsersTableView.dataSource = self
        UsersTableView.delegate = self
        
        observer = ModelEvents.EditCurrentUser.observe{
            self.UsersTableView.refreshControl?.beginRefreshing()
            self.ReloadData()
        }
        
        self.UsersTableView.refreshControl?.beginRefreshing()
        self.ReloadData()
    }
    
    @objc func ReloadData()
    {
        Model.instance.getAllUsers{ (data:[User]?) in
            if (data != nil) {
                self.users = data!
                self.UsersTableView.reloadData()
            }
            self.UsersTableView.refreshControl?.endRefreshing()
        }
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UserViewCell = UsersTableView.dequeueReusableCell(withIdentifier: "UserItem", for: indexPath) as! UserViewCell
        let user = users[indexPath.row]
        cell.username.text = user.username
        cell.beer.text = user.beer
        cell.avatarImg.image = UIImage(named: "avatar")
        if (user.avatarImg != ""){
            cell.avatarImg.kf.setImage(with: URL(string: user.avatarImg))
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "UserViewSegue"){
            let vc : ViewUserViewController = segue.destination as! ViewUserViewController
            vc.user = selectedUser
        }
    }
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath){
        selectedUser = users[indexPath.row]
        performSegue(withIdentifier: "UserViewSegue", sender: self)
    }
    
   
    
}
