//
//  RegisterViewController.swift
//  cheers
//
//  Created by admin on 21/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var cell: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var beer: UITextField!
    @IBOutlet weak var UserPicture: UIImageView!
    @IBOutlet weak var registerSpinner: UIActivityIndicatorView!
    var selectedImage:UIImage?
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var picBtn: UIButton!
    var newUser:User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.username.delegate = self
        self.password.delegate = self
        self.cell.delegate = self
        self.city.delegate = self
        self.beer.delegate = self
        registerSpinner.isHidden = true;
    }
    
    @IBAction func register(_ sender: Any) {
        
        if (username.text == "" || password.text == "" ||
            cell.text == "" || city.text == "" || beer.text == "" )
        {
            let alert = UIAlertController(title:"Error", message:"Missing new user delails", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"Oops", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else
        {
            let existingUser = Model.instance.getUserById(uname: username.text!)
            
            if (existingUser.username != "")
            {
                let alert = UIAlertController(title:"Error", message:"This username is already taken", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK, I'll change it", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            else
            {
                if let selectedImage = selectedImage{
                    registerSpinner.isHidden = false;
                    registerBtn.isEnabled = false;
                    registerBtn.isEnabled = false;
                    Model.instance.storeImage(image: selectedImage, username: username.text!) {(url) in
                        self.newUser = User(
                        username: self.username.text!,
                        password:self.password.text!,
                        cell: self.cell.text!,
                        city: self.city.text!,
                        beer: self.beer.text!,  avatarImg: url)
                        Model.instance.addUser(user: self.newUser)
                        //self.dismiss(animated: true, completion: nil)
                        
                        Model.instance.deleteCurrentUser()
                        Model.instance.insertCurrentUser(currU: self.newUser)
                        self.transitionToHome()}
                } else {
                    registerSpinner.isHidden = false;
                    registerBtn.isEnabled = false;
                    registerBtn.isEnabled = false;
                    
                    self.newUser = User(
                    username: self.username.text!,
                    password:self.password.text!,
                    cell: self.cell.text!,
                    city: self.city.text!,
                    beer: self.beer.text!,  avatarImg: "")
                    Model.instance.addUser(user: self.newUser)
                    
                    Model.instance.deleteCurrentUser()
                    Model.instance.insertCurrentUser(currU: self.newUser)
                                           self.transitionToHome()
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Choose picture from library
    @IBAction func ChoosePic(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(
        UIImagePickerController.SourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerController.SourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // When user select image in gallery
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage;
        self.UserPicture.image = selectedImage;
        dismiss(animated: true, completion: nil);
    }
    
      // Hide keyboard when user touches outside keyboard
      override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          self.view.endEditing(true)
      }
      
      // Presses return key
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          textField.resignFirstResponder()
          return(true)
      }
    
    func transitionToHome(){
        let homeVC = storyboard?.instantiateViewController(identifier: "TabBarController")
            
            view.window?.rootViewController = homeVC
            view.window?.makeKeyAndVisible()
    }
    
}
