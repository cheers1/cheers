//
//  EditPostViewController.swift
//  cheers
//
//  Created by admin on 28/04/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class EditPostViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentText: UITextField!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var savingSpinner: UIActivityIndicatorView!
    
    var post:Post?
    var EditedPost:Post!
    var selectedImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        savingSpinner.isHidden = true;
        
        dateLabel.text = post?.postTime
        contentText.text = post?.Content
        postImage.image = UIImage(named: "Image")
        
        if(post?.postImage != "")
        {
            postImage.kf.setImage(with: URL(string: post!.postImage))
        }
        
        self.contentText.delegate = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func savePost(_ sender: Any) {
        
        let newContent = contentText.text!
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, hh:mm:ss a"
        let dateString = formatter.string(from: Date())
        
         let currUser = Model.instance.getCurrentUser()
         let currUname = currUser.username

         if let selectedImage = selectedImage{
                   savingSpinner.isHidden = false;
                   saveBtn.isEnabled = false;
                   imageBtn.isEnabled = false;
            
                   Model.instance.storeImage(image: selectedImage, username: currUname) {(url) in
                    self.EditedPost = Post(username: currUname, time: dateString, content: newContent, image: url, unixtime: 0)
                    Model.instance.updatePost(post: self.EditedPost, id: self.post!.postId)
                    
                    ModelEvents.DeletePostNotification.post()
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        else
        {
            savingSpinner.isHidden = false;
            saveBtn.isEnabled = false;
            imageBtn.isEnabled = false;
            
            self.EditedPost = Post(username: currUname, time: dateString, content: newContent, image: post!.postImage, unixtime: 0)
            
            Model.instance.updatePost(post: self.EditedPost, id: self.post!.postId)
            
             ModelEvents.DeletePostNotification.post()
            
            self.navigationController?.popViewController(animated: true)
        }
    }
        
     // Hide keyboard when user touches outside keyboard
       override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
       }
       
       // Presses return key
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return(true)
       }
       
    
    @IBAction func changeImage(_ sender: Any) {
         if UIImagePickerController.isSourceTypeAvailable(
               UIImagePickerController.SourceType.photoLibrary) {
                  let imagePicker = UIImagePickerController()
                  imagePicker.delegate = self;
                  imagePicker.sourceType =
                  UIImagePickerController.SourceType.photoLibrary;
                  imagePicker.allowsEditing = true
                  self.present(imagePicker, animated: true, completion: nil)
               }
    }
    
     // When user select image in gallery
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage;
           self.postImage.image = selectedImage;
           dismiss(animated: true, completion: nil);
       }
    
    
    @IBAction func DeletePost(_ sender: Any) {
         let alert = UIAlertController(title: "Are you sure?", message: "Do you want to delete your post?", preferredStyle: .alert)

               alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in self.delete()}))
               alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

               self.present(alert, animated: true)
    }
    
    
     func delete()
       {
          Model.instance.deletePost(postID: self.post!.postId)
          self.navigationController?.popViewController(animated: true)
       }
    
}
