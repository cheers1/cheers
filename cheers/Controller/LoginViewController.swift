//
//  LoginViewController.swift
//  cheers
//
//  Created by admin on 21/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.username.delegate = self
        self.password.delegate = self
        
        let prevUser = Model.instance.getCurrentUser()
        if prevUser.username != ""
        {
            
            let homeVC = storyboard?.instantiateViewController(identifier: "TabBarController") as! UIViewController
            
            present(homeVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func register(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let registerVC = storyBoard.instantiateViewController(identifier: "RegisterViewController")
        
        present(registerVC, animated: true, completion: nil)
    }
    
    @IBAction func Connect(_ sender: Any) {
        if (username.text == "" || password.text == "")
        {
            let alert = UIAlertController(title:"Error", message:"Missing connection delails", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else
        {
            var currUser = User(username: "", password: "", cell: "", city: "", beer: "", avatarImg: "")
            
            Model.instance.VerifyUser(username: username.text!, callback : { (data:User?) in
                if (data != nil)
                {
                    currUser = data!
                }
                
                if ((currUser.username == "") || (currUser.password != self.password.text))
                {
                    let alert = UIAlertController(title:"Error", message:"Wrong username or password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
                else if (currUser.password == self.password.text)
                {
                    Model.instance.deleteCurrentUser()
                    Model.instance.insertCurrentUser(currU: currUser)
                    
                    self.transitionToHome()
                };
            })
        }
    }
    
    // Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Presses return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return(true)
    }
    
    func transitionToHome(){
        let homeVC = storyboard?.instantiateViewController(identifier: "TabBarController") as! UIViewController
        
        view.window?.rootViewController = homeVC
        view.window?.makeKeyAndVisible()
    }
}
