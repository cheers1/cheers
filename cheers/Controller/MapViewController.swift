//
//  MapViewController.swift
//  cheers
//
//  Created by admin on 21/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var observer: Any?
    var selectedUser:User?
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        let currentUser = Model.instance.getCurrentUser()
        initialLocation(user: currentUser)
        getAndPinsAllUsers()
        
//        self.mapView.centerToLocation(CLLocation(latitude: latitude!, longitude: longitude!))
        
        // Observer that listens to current user change details.
        // Need to implement some reload for tha map or data while user is chanched
        observer = ModelEvents.EditCurrentUser.observe{
            self.LoadMap()
        }
//        mapView.delegate = self
    }
    
    
    func LoadMap(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        let currentUser = Model.instance.getCurrentUser()
        self.initialLocation(user: currentUser)
        self.getAndPinsAllUsers()
    }
    
    func getAndPinsAllUsers() {
        Model.instance.getAllUsers{ (data:[User]?) in
            if (data != nil) {
                let users = data!
                
                for user in users{
                    self.addAnnotationInMap(user: user)
                }
            }
        }
    }
    
    @IBAction func RefrehMap(_ sender: Any) {
        LoadMap()
    }
    
    
    func addAnnotationInMap(user: User){
        let cityName = MKLocalSearch.Request()
        cityName.naturalLanguageQuery = user.city
        
        let activeCity = MKLocalSearch(request: cityName)
        
        activeCity.start{ (response, err) in
            if(response == nil){
                print("ERROR")
            } else {
                let latitude = response?.boundingRegion.center.latitude
                let longitude = response?.boundingRegion.center.longitude
                
                let annotation = MyPointAnnotation(user: user, title: user.username, coordinate: CLLocationCoordinate2DMake(latitude!, longitude!))
                self.mapView.addAnnotation(annotation)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let vc : ViewUserViewController = segue.destination as! ViewUserViewController
        vc.user = selectedUser
    }
    

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        var selectedAnnotation: MyPointAnnotation? = view.annotation as? MyPointAnnotation
        selectedUser = selectedAnnotation?.user
        performSegue(withIdentifier: "mapToUserView", sender: self)
    }
    
    func initialLocation(user: User) {
        let cityName = MKLocalSearch.Request()
        cityName.naturalLanguageQuery = user.city
        
        let activeCity = MKLocalSearch(request: cityName)
        
        activeCity.start{ (response, err) in
            if(response == nil){
                print("ERROR")
            } else {
                let latitude = response?.boundingRegion.center.latitude
                let longitude = response?.boundingRegion.center.longitude
                
                self.mapView.centerToLocation(CLLocation(latitude: latitude!, longitude: longitude!))
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class MyPointAnnotation: NSObject, MKAnnotation {
    var user: User!
    var title: String?
    var coordinate: CLLocationCoordinate2D
    
    init(user: User, title: String, coordinate: CLLocationCoordinate2D) {
        self.user = user
        self.title = title
        self.coordinate = coordinate
    }
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 20000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}
