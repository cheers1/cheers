//
//  ViewUserViewController.swift
//  cheers
//
//  Created by admin on 21/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Kingfisher

class ViewUserViewController: UIViewController {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var beer: UILabel!
    @IBOutlet weak var cell: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    
    var user:User?
    var observer: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        username.text = "Meet " + user!.username + "!"
        city.text = user?.city
        beer.text = user?.beer
        cell.text = user?.cell
        avatarImg.image = UIImage(named: "avatar")
        
        if (user?.avatarImg != ""){
            avatarImg.kf.setImage(with: URL(string: user!.avatarImg))
        }
        
        observer = ModelEvents.EditCurrentUser.observe{
            Model.instance.VerifyUser(username: self.user!.username, callback : { (data:User?) in
            if (data != nil)
            {
                self.user = data!
            }
            
//            self.user = Model.instance.getUserById(uname: self.user!.username)
            
            self.username.text = self.user?.username
            self.city.text = self.user?.city
            self.beer.text = self.user?.beer
            self.cell.text = self.user?.cell
        })
    }
    }
    @IBAction func CopyCellNum(_ sender: Any) {
        UIPasteboard.general.string = user?.cell
        let alert = UIAlertController(title:"Cheers!", message:"Cellphone number has been copied to clipboard!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"Cool", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
