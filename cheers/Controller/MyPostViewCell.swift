//
//  MyPostViewCell.swift
//  cheers
//
//  Created by admin on 23/04/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class MyPostTableViewCell: UITableViewCell {
    
    var postId:String!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var postContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        
    
}
