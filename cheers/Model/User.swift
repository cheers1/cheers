//
//  User.swift
//  cheers
//
//  Created by admin on 30/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import Firebase

class User {
    var username:String = ""
    var password:String = ""
    var cell:String = ""
    var city:String = ""
    var beer:String = ""
    var avatarImg:String = ""
    var lastUpdate:Int64?
    
    init(username:String, password:String, cell:String, city:String, beer:String, avatarImg:String ){
        self.username = username
        self.password = password
        self.cell = cell
        self.city = city
        self.beer = beer
        self.avatarImg = avatarImg
    }
    
    init(json:[String:Any]){
        self.username = json["username"] as! String;
        self.password = json["password"] as! String;
        self.cell = json["cell"] as! String;
        self.city = json["city"] as! String;
        self.beer = json["beer"] as! String;
        self.avatarImg = json["avatarImg"] as! String;
        let ts = json["lastUpdate"] as! Timestamp
        self.lastUpdate = ts.seconds
    }
    
    func toJson() -> [String:Any] {
        var json = [String:Any]();
        json["username"] = username
        json["password"] = password
        json["cell"] = cell
        json["city"] = city
        json["beer"] = beer
        json["avatarImg"] = avatarImg
        json["lastUpdate"] = Firebase.Timestamp()
        return json;
    }
}
