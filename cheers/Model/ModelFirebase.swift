//
//  ModelFirebase.swift
//  cheers
//
//  Created by admin on 30/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import Firebase

class ModelFirebase{
    
    func addUser(user:User) {
        let db = Firestore.firestore()
        
        let json = user.toJson();
        db.collection("users").document(user.username).setData(json){
            err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
    }
   
    func getAllUsers(since: Int64, callback: @escaping ([User]?)->Void){
        let db = Firestore.firestore()
        
         db.collection("users").order(by: "lastUpdate").start(after: [Timestamp(seconds: since, nanoseconds: 0)]).getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                callback(nil)
            } else {
                var data = [User]();
                for document in querySnapshot!.documents {
                     if let ts = document.data()["lastUpdate"] as? Timestamp{
                        let tsDate = ts.dateValue();
                        print("\(tsDate)");
                        let tsDouble = tsDate.timeIntervalSince1970;
                        print("\(tsDouble)");

                    }
                    data.append(User(json: document.data()));
                }
                callback(data);
            }
        };
    }
    
   // func updateUser(user:User)
   // {
   //    let db = Firestore.firestore()
        
        // update the user fields
    //    let userRef = db.collection("users").document(user.username)

    //    userRef.updateData(user.toJson()) { err in
    //       if let err = err {
     //          print("Error updating document: \(err)")
     //      } else {
     //          print("Document successfully updated")
    //       }
    //   }

   //  }
    
    func verifyUser(username: String, callback: @escaping (User?)->Void){
        let db = Firestore.firestore()
        
        let docRef = db.collection("users").document(username)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let user = User(json: document.data()!)
               print("User exist: \(user.username)")
               callback(user);
            } else {
                print("Document does not exist")
                callback(nil)
            }
        }
     }
    
    
    func addPost(post:Post) {
       let db = Firestore.firestore()
        let json = post.toJson()
        
       // Add a new post with ID
       
        db.collection("posts").document(post.postId).setData(json){ err in
               if let err = err {
                   print("Error adding document: \(err)")
               } else {
                   print("Document successfully written!")
                   ModelEvents.NewPostNotification.post()
               }
           }
    }
    
    func getAllPosts(since:Int64, callback: @escaping ([Post]?)->Void){
        let db = Firestore.firestore()
          
        db.collection("posts").order(by: "lastUpdate").start(after: [Timestamp(seconds: since, nanoseconds: 0)]).getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                callback(nil)
            } else {
                var data = [Post]();
                for document in querySnapshot!.documents {
                    if let ts = document.data()["lastUpdate"] as? Timestamp{
                        let tsDate = ts.dateValue();
                        print("\(tsDate)");
                        let tsDouble = tsDate.timeIntervalSince1970;
                        print("\(tsDouble)");

                    }
                    data.append(Post(json: document.data()));
                }
                callback(data);
            }
        };
    }
    
    func deleteUser(username: String) {
        let db = Firestore.firestore()
        
        db.collection("users").document(username).delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                print("Document successfully removed!")
            }
        }
    }
    
     func deletePost(postID: String) {
           let db = Firestore.firestore()
           
           db.collection("posts").document(postID).delete() { err in
               if let err = err {
                   print("Error removing document: \(err)")
               } else {
                   print("Document successfully removed!")
               }
           }
       }
    
    
        func addUserToDelete(username:String) {
        let db = Firestore.firestore()
        
            db.collection("usersToDelete").document(username).setData(["username" : username, "lastUpdate" : Firebase.Timestamp()]){
            err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                //odelEvents.DeletePostNotification.post()
            }
        }
    }
    
    func addPostToDelete(postId:String) {
        let db = Firestore.firestore()
        
            db.collection("postsToDelete").document(postId).setData(["postID" : postId, "lastUpdate" : Firebase.Timestamp()]){
            err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                ModelEvents.DeletePostNotification.post()
            }
        }
    }
    
     func getAllPostsToDelete(since:Int64, callback: @escaping ([Post]?)->Void){
           let db = Firestore.firestore()
             
           db.collection("postsToDelete").order(by: "lastUpdate").start(after: [Timestamp(seconds: since, nanoseconds: 0)]).getDocuments { (querySnapshot, err) in
               if let err = err {
                   print("Error getting documents: \(err)")
                   callback(nil)
               } else {
                   var data = [Post]();
                   for document in querySnapshot!.documents {
                       if let ts = document.data()["lastUpdate"] as? Timestamp{
                           let tsDate = ts.dateValue();
                           print("\(tsDate)");
                           let tsDouble = tsDate.timeIntervalSince1970;
                           print("\(tsDouble)");

                       }
                    
                     let post = Post(username: "", time: "", content: "", image: "", unixtime: 0)
                                       
                    let ts = document.data()["lastUpdate"] as! Timestamp
                    post.lastUpdate = ts.seconds
                    
                    post.postId = document.data()["postID"] as! String
                    
                    data.append(post);
                    
                   }
                
                   callback(data);
               }
           };
       }
    
    func getAllUsersToDelete(since:Int64, callback: @escaping ([User]?)->Void){
          let db = Firestore.firestore()
            
          db.collection("usersToDelete").order(by: "lastUpdate").start(after: [Timestamp(seconds: since, nanoseconds: 0)]).getDocuments { (querySnapshot, err) in
              if let err = err {
                  print("Error getting documents: \(err)")
                  callback(nil)
              } else {
                  var data = [User]();
                  for document in querySnapshot!.documents {
                      if let ts = document.data()["lastUpdate"] as? Timestamp{
                          let tsDate = ts.dateValue();
                          print("\(tsDate)");
                          let tsDouble = tsDate.timeIntervalSince1970;
                          print("\(tsDouble)");

                      }
                    
                    let user = User(username: document.data()["username"] as! String , password: "", cell: "", city: "", beer: "", avatarImg: "")
                    
                    let ts = document.data()["lastUpdate"] as! Timestamp
                    user.lastUpdate = ts.seconds
                    
                      data.append(user);
                  }
               
                  callback(data);
              }
          };
      }
}
