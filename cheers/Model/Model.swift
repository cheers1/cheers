//
//  Model.swift
//  cheers
//
//  Created by admin on 30/03/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import UIKit

class Model {
    
    static let instance = Model()
    var modelSql: ModelSql = ModelSql()
    var modelFirebase: ModelFirebase = ModelFirebase()
    
    // 1. Init:
    //  Syncronise local DB with firbase
    // Users- sync from last update
    // Posts- sync from last update
    // Current user- Dont touch (we want that the last login will be keeped
    // last updates- update by this time after we sync
    private init(){
        
    }
    
    // 4. Add new user
    // Adding user after registration
    // Need to check if username does not exist.
    // Add it to firbase and sync the SQL
    // need to check what to do with the picture- later
    func addUser(user:User){
       // modelSql.addUser(user: user)
        modelFirebase.addUser(user: user)
    }
    
    // 9. Get all users
    // Select all the users in asc order by names
    // Select from local DB
    func getAllUsers(callback:@escaping ([User]?)->Void){
        // get the local last update date
        var lud = modelSql.getLastUpdateDate(name: "USERS")
        
        //get the records from firebase since the local last update date
        modelFirebase.getAllUsers(since:lud) { (users) in
            
            var localLud:Int64 = 0
            //save the new records to the local DB
            for user in users!{
                self.modelSql.addUser(user: user)
                
                if(user.lastUpdate! > localLud){
                    localLud = user.lastUpdate!
                }
            }
            
            //save the new local last update date
            self.modelSql.setLastUpdateDate(name: "USERS", lud: localLud)
            
            // get the local last update date
            lud = self.modelSql.getLastUpdateDate(name: "USERS_TO_DELETE")
            
            //get the records from firebase since the local last update date
            self.modelFirebase.getAllUsersToDelete(since: lud) { (users) in
                
                var localLud:Int64 = 0
                
                //delete the records from the local DB
                for user in users!{
                    self.modelSql.deleteUser(username: user.username)
                    
                     if(user.lastUpdate! > localLud){
                        localLud = user.lastUpdate!
                    }
                }
                
                 // set the local last update date
                self.modelSql.setLastUpdateDate(name: "USERS_TO_DELETE", lud: localLud)
                
                //get the complete data from the local DB
                let completeData = self.modelSql.getAllUsers()
                
                //return the complete data to the caller
                callback(completeData)
            }
        }
    }
    
    // 5. Get all posts
     // Select all the posts in desc order by publish- time
     // Select from local DB
    func getAllPosts(callback:@escaping ([Post]?)->Void){
      
        // get the local last update date
        var lud = modelSql.getLastUpdateDate(name: "POSTS")
               
        //get the records from firebase since the local last update date
        modelFirebase.getAllPosts(since:lud) { (posts) in
                   
            var localLud:Int64 = 0;
            //save the new records to the local DB
            for post in posts!{
                self.modelSql.addPost(post: post)
                       
                if(post.lastUpdate! > localLud){
                    localLud = post.lastUpdate!
                }
            }
                   
            //save the new local last update date
            self.modelSql.setLastUpdateDate(name: "POSTS", lud: localLud)
            
            // get the local last update date
            lud = self.modelSql.getLastUpdateDate(name: "POSTS_TO_DELETE")
            
            //get the records from firebase since the local last update date
            self.modelFirebase.getAllPostsToDelete(since: lud) { (posts) in
                
                var localLud:Int64 = 0
                
                //delete the records from the local DB
                for post in posts!{
                    self.modelSql.deletePost(postID: post.postId)
                    
                     if(post.lastUpdate! > localLud){
                        localLud = post.lastUpdate!
                    }
                }
                
                 // set the local last update date
                self.modelSql.setLastUpdateDate(name: "POSTS_TO_DELETE", lud: localLud)
            
                //get the complete data from the local DB
                let completeData = self.modelSql.getAllPosts()
                       
                //return the complete data to the caller
                callback(completeData)
            }
        }
    }
    
     func updateUser(user:User){
          // modelSql.addUser(user: user)
           modelFirebase.addUser(user: user)
       }
    
    func updatePost(post:Post, id:String){
       // modelSql.addUser(user: user)
        post.postId = id
        modelFirebase.addPost(post: post)
    }

    // 6. Add new post
    // Adding post that belong to current user
    // Add it to firbase and sync the SQL
    // need to check what to do with the picture- later
    func addPost(post:Post){
        //modelSql.addPost(post: post)
        modelFirebase.addPost(post: post)
    }
    
    // 10. Get single user details for present
    //  select from the local DB and serch by username
    func getUserById(uname: String)->User{
        return modelSql.getUserById(uname: uname)
    }
    
    func getPostById(pid: String)->Post{
        return modelSql.getPostById(pid: pid)
    }
    
    // 2. Get single user details for connection.
    //  select from the local DB and serch by username and pass
    // if exsit, return the user details
    func VerifyUser(username: String, callback:@escaping(User?)->Void){
        modelFirebase.verifyUser(username: username , callback: callback)
    }
    
    // 3. Insert current user
    // when login, updates with the details of the user we found in the previos function.
    // we will use it in connect
    func insertCurrentUser(currU: User){
        modelSql.insertCurrentUser(user: currU)
    }
    
    func deleteCurrentUser(){
        modelSql.deleteCurrentUser()
    }
    
    func deleteUser(username: String){
        modelSql.deleteUser(username: username)
        modelFirebase.deleteUser(username: username)
        modelFirebase.addUserToDelete(username: username)
    }
    
    func deletePost(postID : String){
        modelSql.deletePost(postID: postID)
        modelFirebase.deletePost(postID: postID)
        modelFirebase.addPostToDelete(postId: postID)
        
    }
    // 7. Get current user details
    // Select from the current_user table in the local DB
    // need to check what to do with the picture- later
    func getCurrentUser()->User{
        return modelSql.getCurrentUser()
    }
    
    //
    func storeImage (image:UIImage, username: String!, callback: @escaping (String)->Void){
        FirebaseStorage.saveImage(image: image, username: username, callback: callback);
    }
}

class ModelEvents{
    static let NewPostNotification = ModelEventsTemplate(name: "NewPostAdded");
    static let EditCurrentUser = ModelEventsTemplate(name: "UserDetailsUpdated");
    static let DeletePostNotification = ModelEventsTemplate(name: "PostRemoved");
    static let EditPostNotification = ModelEventsTemplate(name: "PostDetailsUpdated");
    
    static func removeObserver(observer:Any){
        NotificationCenter.default.removeObserver(observer)
    }
    private init(){}
}

class ModelEventsTemplate{
    let notificationName:String;
    
    init(name:String){
        notificationName = name;
    }
    func observe(callback:@escaping ()->Void)->Any{
        return NotificationCenter.default.addObserver(forName: NSNotification.Name(notificationName),
                                                      object: nil, queue: nil) { (data) in
                                                        callback();
        }
    }
    
    func post(){
        NotificationCenter.default.post(name: NSNotification.Name(notificationName), object: self,userInfo:nil);
    }


}
