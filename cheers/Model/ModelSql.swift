//
//  ModelSql.swift
//  cheers
//
//  Created by admin on 01/04/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation

class ModelSql {
    
    var database: OpaquePointer? = nil
    
    
    
    // Create connection to DB
    init(){
         let dbFileName = "cheers.db"
         if let dir = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first{
            let path = dir.appendingPathComponent(dbFileName)
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK {
                print("Failed to open db file: \(path.absoluteString)")
                return
            }
        }
        
        
        //!!! : Code To recreate Posts Table
//                var sqlite3_stmt: OpaquePointer? = nil
//
//                     if (sqlite3_prepare_v2(database,"DROP TABLE posts",-1,&sqlite3_stmt,nil) == SQLITE_OK){
//
//                         if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
//                              print("current user deleted succefully")          }
//                      }
        
        create()
            
//!!! : Code To recreate Users Table
                
//         var sqlite3_stmt: OpaquePointer? = nil
//
//               if (sqlite3_prepare_v2(database,"DROP TABLE USERS",-1,&sqlite3_stmt,nil) == SQLITE_OK){
//
//                   if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
//                       print("current user deleted succefully")          }
//               }
//
 // !!!! : Code to delete all rows in table

     //    var sqlite3_stmt: OpaquePointer? = nil

            //   if (sqlite3_prepare_v2(database,"DELETE FROM POSTS",-1,&sqlite3_stmt,nil) == SQLITE_OK){

//                   if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
 //                      print("current user deleted succefully")
 //                  }
 //              }
//setLastUpdateDate(name: "POSTS", lud: 0)
//        setLastUpdateDate(name: "USERS", lud: 0)
    //        setLastUpdateDate(name: "USERS_TO_DELETE", lud: 0)
       //     setLastUpdateDate(name: "POSTS_TO_DELETE", lud: 0)
    }
    
    // Create tables
    func create(){
            var errormsg: UnsafeMutablePointer<Int8>? = nil

        var res = sqlite3_exec(database,"CREATE TABLE IF NOT EXISTS USERS (USERNAME TEXT PRIMARY KEY, PASSWORD TEXT, CELL TEXT, CITY TEXT, BEER TEXT, AVATARIMG TEXT)", nil, nil, &errormsg);
        if(res != 0){
            print("error creating users table");
            return
        }
        
        res = sqlite3_exec(database,"CREATE TABLE IF NOT EXISTS POSTS (PID TEXT PRIMARY KEY,USERNAME TEXT, CONTENT TEXT, POSTTIME TEXT, POSTIMAGE TEXT, UNIXTIME NUMBER)", nil, nil, &errormsg);
        if(res != 0){
            print("error creating posts table");
            return
        }
        
         res = sqlite3_exec(database,"CREATE TABLE IF NOT EXISTS CURRENT_USER (USERNAME TEXT PRIMARY KEY, PASSWORD TEXT, CELL TEXT, CITY TEXT, BEER TEXT,                             AVATARIMG TEXT)", nil, nil, &errormsg);
        if(res != 0){
            print("error creating current user table");
            return
        }
        
         res = sqlite3_exec(database,"CREATE TABLE IF NOT EXISTS LAST_UPDATE_DATE (NAME TEXT PRIMARY KEY, LUD NUMBER)", nil, nil, &errormsg);
        
               if(res != 0){
                   print("error creating last update date table");
                   return
               }
    }
    
    func addUser(user:User){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO USERS(USERNAME, PASSWORD, CELL, CITY, BEER, AVATARIMG) VALUES (?,?,?,?,?,?);",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let username = user.username.cString(using: .utf8)
            let password = user.password.cString(using: .utf8)
            let cell = user.cell.cString(using: .utf8)
            let city = user.city.cString(using: .utf8)
            let beer = user.beer.cString(using: .utf8)
            let avatarImg = user.avatarImg.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, username,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, password,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, cell,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, city,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, beer,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 6, avatarImg,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new user added succefully")
            }
        }
    }
       
    func getAllUsers()->[User]{
        var sqlite3_stmt: OpaquePointer? = nil
        var data = [User]()
        
        if (sqlite3_prepare_v2(database,"SELECT * from USERS;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let username = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let password = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let cell = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                let city = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let beer = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                let avatarImg = String(cString:sqlite3_column_text(sqlite3_stmt,5)!)
                
                data.append(User(username: username, password: password, cell: cell, city: city, beer: beer, avatarImg: avatarImg ))
            }
         }
        
         sqlite3_finalize(sqlite3_stmt)
         return data
    }
    
    func getAllPosts()->[Post]{
        var sqlite3_stmt: OpaquePointer? = nil
        var data = [Post]()
        
        if (sqlite3_prepare_v2(database,"SELECT * from POSTS ORDER BY UNIXTIME DESC;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let Pid = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let Pusername = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let Pcontent = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                let Ptime = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let Pimage = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                
                let Punixtime = sqlite3_column_double(sqlite3_stmt, 5)
                
                let post = Post(username: Pusername, time: Ptime, content: Pcontent, image: Pimage, unixtime: Double(Punixtime))
                
                post.postId = Pid

                data.append(post)
            }
         }
        
         sqlite3_finalize(sqlite3_stmt)
         return data
    }
    
    func addPost(post:Post){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO POSTS(PID, USERNAME, CONTENT, POSTTIME, POSTIMAGE, UNIXTIME) VALUES (?,?,?,?,?,?);",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let postId = post.postId.cString(using: .utf8)
            let username = post.username.cString(using: .utf8)
            let content = post.Content.cString(using: .utf8)
            let postTime = post.postTime.cString(using: .utf8)
            let image = post.postImage.cString(using: .utf8)
            let unixTime = post.UnixPostTime
            
            sqlite3_bind_text(sqlite3_stmt, 1, postId,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, username,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, content,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, postTime,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, image,-1,nil);
            sqlite3_bind_double(sqlite3_stmt, 6, unixTime);
            
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new user added succefully")
            }
        }
    }
    
    func getUserById(uname: String)->User{
        var sqlite3_stmt: OpaquePointer? = nil
        var user = User(username: "", password: "", cell: "", city: "", beer: "", avatarImg: "" )
        
        if (sqlite3_prepare_v2(database,"SELECT * from USERS WHERE USERNAME = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let username = uname.cString(using: .utf8)
            sqlite3_bind_text(sqlite3_stmt, 1, username,-1,nil);


            if (sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let username = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let password = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let cell = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                let city = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let beer = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                let avatarImg = String(cString:sqlite3_column_text(sqlite3_stmt,5)!)
                
                user = User(username: username, password: password, cell: cell, city: city, beer: beer, avatarImg: avatarImg )
            }
         }
        
         sqlite3_finalize(sqlite3_stmt)
        
        
        return user
    }
    
    func VerifyUser(uname: String, pass: String)->User{
        var sqlite3_stmt: OpaquePointer? = nil
        var user = User(username: "", password: "", cell: "", city: "", beer: "", avatarImg: "" )
        
        if (sqlite3_prepare_v2(database,"SELECT * from USERS WHERE USERNAME = ? AND PASSWORD = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let username = uname.cString(using: .utf8)
            let password = pass.cString(using: .utf8)

            sqlite3_bind_text(sqlite3_stmt, 1, username,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, password,-1,nil);


            if (sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let username = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let password = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let cell = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                let city = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let beer = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                let avatarImg = String(cString:sqlite3_column_text(sqlite3_stmt,5)!)
                
                user = User(username: username, password: password, cell: cell, city: city, beer: beer, avatarImg: avatarImg )
            }
         }
        
         sqlite3_finalize(sqlite3_stmt)
        
        return user
    }
    
    func insertCurrentUser(user:User){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO CURRENT_USER(USERNAME, PASSWORD, CELL, CITY, BEER, AVATARIMG) VALUES (?,?,?,?,?,?);",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let username = user.username.cString(using: .utf8)
            let password = user.password.cString(using: .utf8)
            let cell = user.cell.cString(using: .utf8)
            let city = user.city.cString(using: .utf8)
            let beer = user.beer.cString(using: .utf8)
            let avatarImg = user.avatarImg.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, username,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, password,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, cell,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, city,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, beer,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 6, avatarImg,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("current user changed succefully")
            }
        }
    }
    
    func deleteCurrentUser(){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"DELETE FROM CURRENT_USER;",-1,&sqlite3_stmt,nil) == SQLITE_OK){

            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("current user deleted succefully")
            }
        }
    }
    
    func getCurrentUser()->User{
        var sqlite3_stmt: OpaquePointer? = nil
        var user = User(username: "", password: "", cell: "", city: "", beer: "", avatarImg: "" )
        
        if (sqlite3_prepare_v2(database,"SELECT * from CURRENT_USER;",-1,&sqlite3_stmt,nil) == SQLITE_OK){

            if (sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let username = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let password = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let cell = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                let city = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let beer = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                let avatarImg = String(cString:sqlite3_column_text(sqlite3_stmt,5)!)
                
                user = User(username: username, password: password, cell: cell, city: city, beer: beer, avatarImg: avatarImg )
            }
         }
        
         sqlite3_finalize(sqlite3_stmt)
        
        
        return user
    }
    
    
    func setLastUpdateDate(name: String, lud:Int64){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO LAST_UPDATE_DATE(NAME, LUD) VALUES (?,?);",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, name.cString(using: .utf8),-1,nil);
            sqlite3_bind_int64(sqlite3_stmt, 2, lud);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new last update date added succefully \(lud)")
            }
        }
        
         sqlite3_finalize(sqlite3_stmt)
        
    }
    
    func getLastUpdateDate(name: String)->Int64{
        var sqlite3_stmt: OpaquePointer? = nil
        var lud:Int64 = 0
        
        if (sqlite3_prepare_v2(database,"SELECT * from LAST_UPDATE_DATE where NAME like ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, name,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                 lud = Int64(sqlite3_column_int64(sqlite3_stmt,1))
               
            }
         }
        
        sqlite3_finalize(sqlite3_stmt)
        print("get last update date succefully \(lud)")
        return lud
    }
    
    func deleteUser(username: String){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"DELETE FROM USERS where USERNAME = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let username = username.cString(using: .utf8)
            sqlite3_bind_text(sqlite3_stmt, 1, username,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("current user deleted succefully")
            }
        }
    }
    
    func deletePost(postID: String){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"DELETE FROM POSTS where PID = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            let pid = postID.cString(using: .utf8)
            sqlite3_bind_text(sqlite3_stmt, 1, pid,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("current post deleted succefully")
            }
        }
    }
    
     func getPostById(pid: String)->Post{
           var sqlite3_stmt: OpaquePointer? = nil
        var post = Post(username: "", time: "", content: "", image: "", unixtime: 0)
           
           if (sqlite3_prepare_v2(database,"SELECT * from POSTS WHERE PID = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
               
               let postId = pid.cString(using: .utf8)
               sqlite3_bind_text(sqlite3_stmt, 1, postId,-1,nil);


               if (sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                   let username = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                   let postTime = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                   let content = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let  postImage = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                
                let unixTime = Double(sqlite3_column_double(sqlite3_stmt,5))
                   
                post = Post(username: username, time: postTime, content: content, image: postImage, unixtime: unixTime)
               }
            }
           
            sqlite3_finalize(sqlite3_stmt)
           
           
           return post
       }
    
}
