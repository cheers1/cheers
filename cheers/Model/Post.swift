 //
//  Post.swift
//  cheers
//
//  Created by admin on 02/04/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import Firebase
 
 class Post {
     var postId = ""
     var username:String = ""
     var postTime:String = ""
     var Content:String = ""
     var postImage:String = ""
     var UnixPostTime:Double = 0
     var lastUpdate:Int64?
    
    init(username:String, time:String, content:String, image: String, unixtime: Double){
         self.username = username
         self.postTime = time
         self.Content = content
         self.postImage = image
         self.postId = username + " " + time
        if (unixtime == 0){
            self.UnixPostTime = NSDate().timeIntervalSince1970
        }
        else{
            self.UnixPostTime = unixtime
        }
     }
    
    init(json:[String:Any]){
        self.postId = json["postId"] as! String
        self.username = json["username"] as! String;
        self.postTime = json["postTime"] as! String;
        self.Content = json["Content"] as! String;
        let ts = json["lastUpdate"] as! Timestamp
        self.lastUpdate = ts.seconds
        self.postImage = json["postImage"] as! String;
        self.UnixPostTime = json["UnixPostTime"] as! Double;
        
    }
    
    func toJson() -> [String:Any] {
        var json = [String:Any]();
        json["postId"] = postId
        json["username"] = username
        json["postTime"] = postTime
        json["Content"] = Content
        json["lastUpdate"] = Firebase.Timestamp()
        json["postImage"] = postImage
        json["UnixPostTime"] = UnixPostTime
        
        return json;
    }
    
 }

